const cardOutline = require("../assets/icons/card-outline.png") as string;
const card = require("../assets/icons/card.png") as string;
const dashboardOutline = require("../assets/icons/dashboard-outline.png") as string;
const dashboard = require("../assets/icons/dashboard.png") as string;
const moneyBagOutline = require("../assets/icons/money-bag-outline.png") as string;
const moneyBag = require("../assets/icons/money-bag.png") as string;
const setting = require("../assets/icons/setting.png") as string;
const waves = require("../assets/icons/waves.png") as string;
const search = require("../assets/icons/seearch.svg") as string;
const menu = require("../assets/icons/menu.svg") as string;
const faq = require("../assets/icons/faq.svg") as string;
const rating = require("../assets/icons/rating.svg") as string
const star = require("../assets/icons/start.png") as string
const share = require("../assets/icons/share.png") as string
const instagram = require("../assets/icons/Instagram.png") as string
const icons = {
    card,
    cardOutline,
    dashboard,
    dashboardOutline,
    moneyBag,
    moneyBagOutline,
    setting,
    waves,
    search,
    faq,
    menu,
    rating, star,share, instagram
}

export default icons